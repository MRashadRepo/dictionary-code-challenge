/**
 * Module dependencies
 */
const ft = require('./fileTools');
const fs   = require('fs');
const JF = require('jsonfile');
const cliStyles = require('./cliStyles');


/**
 * Add a word to Dictionary JSON File
 * @param {string} path
 * @param {array} fields
 * @param {function} cb
 */
function addWords(path, fields, cb) {
    const file = path + '/dictionary/dictionary.json';
    ft.createDirIfIsNotDefined(path, 'dictionary', function () {
        if (fs.existsSync(file)) {
            JF.readFile(file, function(err, obj) {
                // Loop through array of words provided in <fields> array
                for (let index=0; index < fields.length; index++) {
                let found = false;
                // Loop through all the objects in the array
                for (i=0;i < obj.length; i++) {
                    // Check each key against the fields
                    if (obj[i].key !== fields[index].key) {
                      found = false;
                    }else if (obj[i].key == fields[index].key){
                      found = true;
                      break;                      
                    }
                  }
                // if we can't find it, append it to the file
                if(!found){
                    obj.push(fields[index]);                  
                    fs.writeFile(file, JSON.stringify(obj), function (err) {
                      if (err) throw err;
                      console.log(cliStyles.green + 'could not find in dictionary <' + fields[index].key + '> so adding it...' + cliStyles.reset);
                  });
                } else {
                    console.log(cliStyles.red + 'found it. Word <'+ obj[i].key + '> already exist.' + cliStyles.reset);
                }
             }
            })
            // return cb(null, "DONE");
        } else {
            ft.writeFile(path + '/dictionary/dictionary.json', JSON.stringify(fields), null, cb);
        }
    });
}

/**
 * Get word from Dictionary JSON File by Key
 * @param {string} path
 * @param {array} key
 * @param {function} cb
 */
function removeWordByKey(path, key, cb) {
    const file = path + '/dictionary/dictionary.json';
    let found = false;
    if (fs.existsSync(file)) {
        JF.readFile(file, function(err, obj) {
            // Loop through all the objects in the array
            for (i=0;i < obj.length; i++) {
                  // Check each key against the fields
                if (obj[i].key == key){
                found = true;
                obj.splice(i, 1);
                fs.writeFile(file, JSON.stringify(obj), function (err) {
                  if (err) throw err;
                  return cb(false, 'found it and removed...' + JSON.stringify({ key: key }));
              });
              }
            }
            // if we can't find it, append it to the file
            if(!found){
              return cb(true, 'could not find it...Try another word');
            }
          })
    } else {
        return cb(true, 'File doesnt exist....');
    }
}

/**
 * Remove word from Dictionary JSON File by Key
 * @param {string} path
 * @param {array} key
 * @param {function} cb
 */
function getWordByKey(path, key, cb) {
    const file = path + '/dictionary/dictionary.json';
    let found = false;
    if (fs.existsSync(file)) {
        JF.readFile(file, function(err, obj) {
            // Loop through all the objects in the array
            for (i=0;i < obj.length; i++) {
                  // Check each key against the fields
                if (obj[i].key == key){
                found = true;
                return cb(false, 'found it...' + JSON.stringify({ key: obj[i].key, value: obj[i].value }));
              }
            }
            // if we can't find it, append it to the file
            if(!found){
              return cb(true, 'could not find it...Try another word');
            }
          })
    } else {
        return cb(true, 'File doesnt exist....');
    }
}

/**
 * List all words from Dictionary JSON File
 * @param {string} path
 * @param {function} cb
 */
function listAllWords(path, cb) {
    const file = path + '/dictionary/dictionary.json';
    let found = false;
    if (fs.existsSync(file)) {
        JF.readFile(file, function(err, obj) {
            return cb(false, JSON.stringify(obj));
          })
    } else {
        return cb(true, 'File doesnt exist....');
    }
}

/**
 * Clear Dictionary JSON File
 * @param {string} path
 * @param {function} cb
 */
function clearDictionary(path, cb) {
    const filePath = path + '/dictionary/dictionary.json';
    if (fs.existsSync(filePath)) {
        fs.truncate(filePath, 0, function(){return cb(false, 'Dictionary Content Cleared...');})
    } else {
        return cb(true, 'File doesnt exist....');
    }
}

module.exports = {
    addWords,
    getWordByKey,
    removeWordByKey,
    listAllWords,
    clearDictionary
};
