# take default image of node boron i.e  node 8.x
FROM node:6.10.1

# create app directory in container
RUN mkdir -p /app

# set /app directory as default working directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# copy all file from current dir to /app in container
COPY . /app/

# expose port 8080
EXPOSE 8080

# cmd to start service
CMD [ "npm", "start" ]
