# Simple JS/Node Developer Challenge

# Store CLI Help
```sh
# Store Help
cd ./bin
node store -h or --help
  Usage: store [options]

  Options:

    -V, --version       output the version number
    -a, --add <fields>  dictionary fields for each word provided like this => key1:value1,key2:value2
    -l, --list          list of words in dictionary
    -g, --get <key>     get word in dictionary by key
    -r, --remove <key>  remove word from dictionary by key
    -c, --clear         clear all words in dictionary
    -h, --help          output usage information

```

# Store CLI runNonInteractiveMode
```sh
node store -a section:"division or part or portion",discovery:"detection or find or invention"
node store -g section
node store -r section
node store -c
```

# Store CLI runInteractiveMode
```sh
cd ./bin
./store or just run store.sh
```
### Terminal
```sh
Key (press <enter> to stop adding words) : special
Value: particular
Key (press <enter> to stop adding words) : 
```
### Output
```sh
{ fields: [ { key: 'special', value: 'particular' } ] }
could not find in dictionary <special> so adding it...
```

# Docker Steps Screenshots
```sh
# Docker List of Containers
docker ps
```
```sh
# Docker Images
docker images
```

```sh
# Docker web-app Build
docker build -t <username>/node-web-app
```
![1_-_docker-build](/uploads/10fb4dfe256ea60b8fa97f974bb056d4/1_-_docker-build.PNG)
![2_-_docker-build-success](/uploads/17d567c8ac13076c24393dbec50b1d1e/2_-_docker-build-success.PNG)

```sh
# Docker Images Created
docker images
```
![3_-_docker-image-created](/uploads/e95469f8cb57a99945d2e9bc9baffcb2/3_-_docker-image-created.PNG)

```sh
# Docker Run Container
docker run -p 49160:8080 -d rashaddocker26/node-web-app
docker logs <container ID>
```
![4_-_docker-run-container-id-logPNG](/uploads/544535b53f9eb6d0a190cf01c7b12da6/4_-_docker-run-container-id-logPNG.PNG)

```sh
# Docker Test Running
curl -i localhost:49160
```
![5_-_docker-test-app](/uploads/59c0967b44e67910bae2de546ead0fb8/5_-_docker-test-app.PNG)

# gitlab-CI/CD
```sh
# Gitlab CI/CD Docker deploy
```
![gitlab-CI](/uploads/73b717ec1ea8743c218868dd9951ef56/gitlab-CI.PNG)

# Goal
Clone this repo and build a simple dictionary key/value store script using only core NodeAPI and ECMAScript 5 or 6.  
Store the key/value dictionary using filesystem.
The client should be a standalone terminal tool.
Commit and track your work history in a new GitLab repo. Once finished email the link to your repo.

# Store Commands

`$ node store.js add mykey myvalue`

`$ node store.js list`

`$ node store.js get mykey`

`$ node store.js remove mykey`

`$ node store.js clear`

# Bonus

- Write clean, modular and testable code.
- Instead of running `node store.js` alter the runtime so it can be run as `./store`.
- Add ability to deploy in Docker container.
- Add GitLab compatible CI/CD to perform a server deploy.